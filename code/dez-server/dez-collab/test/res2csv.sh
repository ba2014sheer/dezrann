#!/bin/sh
echo "nb clients;emitted;accepted;accepted rate;denied;denied rate;missed ack;total time;action time";
cat $1 \
	| awk '{print ""$6";"$9";"$13";"$14";"$18";"$19";"$24";"$1";"$2""}'\
	| sed "s/]//"\
	| sed "s/(//g"\
	| sed "s/)//g" \
	| sed "s/%//g"\
	| sed "s/\./,/g"
