/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/av-icons.js';
import '@polymer/iron-icons/communication-icons.js';
import '@polymer/iron-icons/device-icons.js';
import '@polymer/iron-icons/image-icons.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/iron-meta/iron-meta.js';
import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';
import './dez-acl.js';
import './shared-styles.js';
import './dez-global-variable.js';


class DezCorpusView extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
        div.card h1 a {
          text-decoration: none;
        }
      </style>

      <iron-meta id="config" key="config" value="{{config}}"></iron-meta>

      <template is="dom-repeat" items="{{_pieces}}" as="piece" mutable-data>
        <div class="card">
          <h1>
            <iron-icon icon="image:music-note"></iron-icon>
            <a href="[[rootPath]]piece[[tail.path]]/[[piece.id]]">[[piece.title]]</a>
          </h1>

          <p>[[piece.composer]]</p>
          <iron-icon icon="av:hearing" hidden$="[[!piece.sources.audios]]"></iron-icon>
          <template is="dom-repeat" items="[[piece.sources.images]]" as="image" mutable-data>
            <iron-icon icon="av:queue-music"></iron-icon>
          </template>
          <template is="dom-repeat" items="[[piece.sources.audios]]" as="audio" mutable-data>
            <template is="dom-repeat" items="[[audio.images]]" as="image" mutable-data>
              <iron-icon icon="device:graphic-eq" alt="hop"></iron-icon>
            </template>
          </template>
          <dez-acl acl="[[piece.access]]"></dez-acl>
        </div>
      </template>

      <template is="dom-repeat" items="{{_corpora}}" as="corpus" mutable-data>
        <div class="card">
          <h1>
            <iron-icon icon="av:library-music"></iron-icon>
            <a href="[[rootPath]]corpus[[tail.path]]/[[corpus.name]]">[[corpus.name]]</a>
          </h1>
          <dez-acl acl="[[corpus.access]]"></dez-acl>
        </div>
      </template>

      <iron-ajax
        id="corpusLocationRequest"
        handle-as="json"
        debounce-duration="300"
        on-response="_corpusReady"
        on-error="_handleAjaxError">
      </iron-ajax>

      <app-location route="{{route}}"></app-location>
      <app-route route="{{route}}" pattern="/:page" tail="{{tail}}" on-tail-changed="_handleTailChanged">
      </app-route>

      <dez-global-variable key="userData" value="{{storedUser}}"></dez-global-variable>

    `;
  }

  static get properties() {
    return {
      storedUser: {
        type : Object,
        observer : "_handleStoredUserChanged"
      }
    };
  }

  constructor () {
    super()
    this._bearer = ""
  }

  _generateRequest() {
    this.$.corpusLocationRequest.url = this._url
    this.$.corpusLocationRequest.headers['Authorization'] = this._bearer
    this.$.corpusLocationRequest.generateRequest()
  }

  _handleStoredUserChanged () {
    let bearer = this.storedUser?'Bearer ' + this.storedUser.access_token:""
    if (bearer != this._bearer) {
      this._bearer = bearer
      this._generateRequest()
    }
  }

  _corpusReady (event) {
    if (event.detail.response) { // acl ok
      this._pieces = event.detail.response.pieces
      this._corpora = event.detail.response.corpora
    }
  }

  _handleTailChanged (event) {
    let lastUrl = this._url
    let path = this.route.path.split("/corpus")[1] || ""
    this._url = JSON.parse(this.config).dezws + path
    if (this._url != lastUrl) {
      this._generateRequest()
    }
  }

  _handleAjaxError (event) {
    let response = event.detail.request.xhr.response
    if (response && response.name == "TokenExpiredError") {
      this._bearer = ""
      this.storedUser = null
      this.set('route.path', '/login');
    }
  }

}

window.customElements.define('dez-corpus-view', DezCorpusView);
