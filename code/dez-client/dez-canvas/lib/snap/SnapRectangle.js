class SnapRectangle {

    constructor (rect, marginTop) {
        this._rect = rect;
        this._marginTop = marginTop
    }

    get height () { return parseInt(this._rect.attr("height")) }
    get width () { return parseInt(this._rect.attr("width")) }

    set width (width) { return this._rect.attr({"width": width}) }

    get x () { return parseInt(this._rect.attr("x")); }
    get y () { return parseInt(this._rect.attr("y")); }

    get leftMiddle () { return {x: this.x, y: this.y + this.height/2}; }
    get rightMiddle () { return {x: this.x + this.width, y: this.y + this.height/2}; }

    middle (direction) {
        switch (direction) {
            case "LEFT":
                return this.leftMiddle
                break;
            case "RIGHT":
                return this.rightMiddle
                break;
        }
    }

    redraw (x, y, width, height, color) {
        this._rect.attr({
            "x": x,
            "y": y + this._marginTop,
            "width": width,
            "height": height,
            fill: color
        })
    }

    canContain (box, marginWidth, marginHeight) {
        let height = parseInt(this._rect.attr("height"))
        return this.width > box.width + marginWidth
            && height > box.height + marginHeight
    }

    remove () {
        this._rect.remove();
    }

    set strokeWidth (value) {
        this._rect.attr({strokeWidth: value})
    }

    set opacity (value) {
        this._rect.attr({opacity: value})
    }

    contains (x) {
        let left = this.x;
        let right = this.x + this.width;
        return left <= x && x <= right;
    }

}
