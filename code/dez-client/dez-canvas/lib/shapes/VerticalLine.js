class VerticalLine extends Rectangle {

    constructor (view, resizers, defaults) {
        super(view, resizers, defaults)
        if (defaults && defaults["text"] != undefined) {
            this._DEFAULT_TEXT = defaults["text"];
        } else {
            this._DEFAULT_TEXT = "vline"
        }
        this._textPosition = "bottom"
        this._textStyle = 'vertical-line-text'
        this._WIDTH = 10;
        this._OPACITY = 0.5
        this.MIN_WIDTH = 0;
        this._HORIZONTAL_MARGIN = 0;
        this._REDUCE_HEIGHT = 15
    }

    show (x, y, width, height) {
        super.show(x - this._WIDTH/2, y, this._WIDTH, height - (this.text.length ? this._REDUCE_HEIGHT : 0))
    }

    $placeText (x, y, width, height) {
        if (height == 0) return;
        this._text.moveTo(x + this._WIDTH/2, y + height + this._REDUCE_HEIGHT)
    }

    $showAsSelected () {
        if (this._rect != undefined) this._rect.opacity= 0.8
    }

    showAsUnselected () {
        if (this._rect != undefined) this._rect.opacity= 0.5
    }

}
