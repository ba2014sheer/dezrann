# \<scorecloud\>

Score analysis editor

## Install the Polymer-CLI and other required packages

Polymer-CLI : https://www.npmjs.com/package/polymer-cli

```
$ (sudo) npm install -g polymer-cli
$ (sudo) npm install -g bower
$ bower install snap.svg
$ bower install iron-signals
```


## Viewing Your Application

```
$ polymer serve
```

## Building Your Application

```
$ polymer build
```

This will create a `build/` folder with `bundled/` and `unbundled/` sub-folders
containing a bundled (Vulcanized) and unbundled builds, both run through HTML,
CSS, and JS optimizers.

You can serve the built versions by giving `polymer serve` a folder to serve
from:

```
$ polymer serve build/bundled
```

## Running Tests

```
$ mocha
```

```
$ polymer test
```

Your application is already set up to be tested via [web-component-tester](https://github.com/Polymer/web-component-tester). Run `polymer test` to run your application's test suite locally.


## babel

### Install

#### Base
```
$ sudo npm install -g babel-cli babel-preset-env babel-preset-es2015
```

#### Modules umd
```
$ sudo npm install -g babel-plugin-transform-es2015-modules-umd
```

### Usage

Edit .babelrc file:

```
{
  "plugins": ["transform-es2015-modules-umd"],
  "presets": ["env"]
}
```

Compile:

```
$ babel src/lib/ -d lib/
```
