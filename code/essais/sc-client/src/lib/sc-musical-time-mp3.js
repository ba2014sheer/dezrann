export class MusicalTimeMP3 {

    setMap(map) {
        this.map = map
    }

    setBpb(bpb) {
        this.bpb = bpb
    }

    /**
     * Converts a time in millisecond to a musical time.
     *
     * @param {number} ms Time in millisecond
     */
    toMusicalTime(ms) {

        for (let t in this.map)
            if (this.map[t] > ms)
                return t - 1

        return 0
    }

    /**
     * Converts a musical time to a time in millisecond.
     *
     * @param {number} mt Musical time
     */
    toMillisecond(mt) {

        for (let t in this.map)
            if (t > mt)
                return this.map[t - 1]

        return 0
    }

    /**
     * Returns the first time of a measure from a muscial time.
     *
     * @param {number} mt Muscial time
     */
    firstTimeOfMeasure(mt) {

        return Math.floor(mt / this.bpb) * this.bpb
    }
}
