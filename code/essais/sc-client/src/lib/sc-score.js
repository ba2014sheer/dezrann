/**
 * class managing a musical score
 */
export class Score {

    /**
     * creates a score as svg elements
     * @param {*} width
     * @param {*} height
     * @param {*} naturalHeight real height of the score
     */
    constructor(width,height,naturalHeight) {
        this.width = width;
        this.height = height;
        this.ratio = width/window.innerWidth;
        this.heightRatio = height/naturalHeight;
        this.paper = Snap('#myscore').attr({
                        height: height,
                        width: width,
                    });

        this.group = this.paper.g().attr({transform:'matrix(1,0,0,1,0,0)',id:"main"});
        this.offset = 0;
        this.staffs = [];
    }

    /**
     * sets the staffs for the score
     * @param {*} data JSON encoded data with at least an object staffs
     * @param {*} offset position relative to the page
     */
    setStaff(data) {
        this.data = data;
        for (var i = 0 ; i < data.staffs.length ; i++) {
            this.staffs[i] = new scStaff.Staff(data.staffs[i].top*this.heightRatio,data.staffs[i].bottom*this.heightRatio);
         }
    }

    /**
     * changes the size of the glabs depending on new image sizes
     * @param {*} width new width of the image
     * @param {*} height new height of the image
     */
    adaptGlab(width,height) {
        var ratio = height/this.height;
        this.paper.selectAll('.rect').forEach(function(e){
            var x = e.attr("x")*ratio;
            var y =e.attr("y")*ratio;
            var w = e.attr("width") *ratio;
            var h = e.attr("height")*ratio;
            e.attr({width:w,height:h,x:x,y:y});
        })
    }

    /**
     * change the size of the score
     * @param {*} width new width
     * @param {*} height new height
     * @param {*} imgHeight real height of the image
     */
    resize(width,height,imgHeight) {
        this.adaptGlab(width,height);
        this.width = width;
        this.height = height;
        this.heightRatio = height/imgHeight;
        this.setStaff(this.data);
        this.paper.attr({width:width,height:height});
        Snap('#content').attr({width:width,height:height});
    }

    /**
     * adds an image to this score
     * @param {*} src source of the image
     * @param {*} id
     */
    attachImg(src,id) {
       var img = this.paper.image(src,0,0,this.width,this.height).attr({
           id:id,
       });
       this.group.add(img);
    }

    /**
     * creates a glab for this score
     * @param {*} x
     * @param {*} y
     */
    createGlab(x,y) {
        this.stopSelection();
        for (var i = 0 ; i < this.staffs.length ; i++) {
            var staff = this.staffs[i];
            if (y < staff.getBottom() && y > staff.getTop()) {
                this.glab = this.group.rect(x,staff.getTop()+10,0,staff.getSize()-15,10).attr({
                fill:"red",
                "fill-opacity":0.3,
                class:"rect",
                })
                this.glab.attr({id:this.glab.id})
                return true;
            }
        }
        this.glab = null;
    }

    /**
     * changes the size and eventually the start position of the last glab created
     * @param {*} dx new length
     * @param {*} dy difference of y since the beginning of the glab creation
     * @param {*} x new x position
     */
    drawGlab(dx,dy,x) {
        //TODO : adapter angle à écran => ?
        if (this.calcAngle(dx,dy) > 0.1 && this.calcAngle(dx,dy) < 2.7) {
            return;
        }

        //stabilo de droite à gauche
        if (dx < 0) {
            dx = - dx;
            x -= dx;
        }

        try {
            this.setGlabWidth(dx);
            this.moveX(x);
        }
        catch (e) {

        }
    }

    /**
     * removes the current selected glab from the score
     */
    removeGlab() {
        this.stopSelection();
        this.glab.remove();
    }

    /**
     * highlight the current selected glab
     */
    selectCurrent() {
        this.changeOpacity(0.6);
        this.displayArrows();
    }

    /**
     * selects the glab having id passed as a parameter
     * @param {*} id
     */
    select(id) {
        this.stopSelection();
        this.glab = Snap('#' +id);
        this.selectCurrent();
        this.x = parseInt(this.glab.attr("x"));
        this.y = parseInt(this.glab.attr("y"));
    }

    getCurrentGlabX() {
        return this.x
    }

    getCurrentGlabY() {
        return this.y
    }

    getCurrentGlabWidth() {
        return parseInt(this.glab.attr("width"))
    }

    /**
     * stops highlighting the current selected glab
     */
    stopSelection() {
        try {
            this.changeOpacity(0.3);
            this.paper.selectAll('.arrow').forEach(function(e){
                        e.remove();
                    },this);

            }
        catch (e) {
        }
    }

    /**
     * returns the angle created by the two parameters in rad
     * @param {*} xLength
     * @param {*} yLength
     */
    calcAngle(xLength,yLength) {
        var hyp = Math.sqrt(Math.pow(xLength,2) + Math.pow(yLength,2));
        return Math.acos(xLength/hyp);
    }

    /**
     * creates arrows for the current selected glab
     */
    displayArrows(){
        //TODO : adapter la taille des flèches
        var x = parseInt(this.glab.attr("x")) + 10;
        var mid = parseInt(this.glab.attr("y")) + this.glab.attr("height")/2;
        this.left = new scArrow.Arrow(x,mid,15,"#FFF","left",this.group,this.glab);
        x = parseInt(this.glab.attr("x")) + parseInt(this.glab.attr("width")) -10;
        this.right = new scArrow.Arrow(x,mid,-15,"#FFF","right",this.group,this.glab);
    }

    //Modification des rectangles

    /**
     * set the current selected glab's width
     * @param {*} width
     */
    setGlabWidth(width) {
        this.glab.attr({
            width:width,
        })
    }

    /**
     * sets the current selected glab opacity
     * @param {*} opacity
     */
    changeOpacity(opacity) {
        this.glab.attr({
            "fill-opacity":opacity
        })
    }

    /**
     * sets the start x position of the current selected glab
     * @param {*} x
     */
    moveX(x) {
        this.glab.attr({
            x:x
        })
    }

    /**
     * sets the y position of the current selected glab
     * @param {*} y
     */
    moveY(y) {
        this.glab.attr({
            y:y
        })
    }

    /**
     * change the size of the glab designated by id
     * @param {*} x
     * @param {*} y
     * @param {*} id
     */
    resizeGlab(x,y,id) {
        if (id == "left")
            this.moveLeft(x,y);

        if (id =="right")
            this.moveRight(x,y);
    }

    /**
     * moves the left arrow of the current selected glab and resize the glab
     * @param {*} x
     * @param {*} y
     */
    moveLeft(x,y) {
        var xLength = parseInt(x) - parseInt(this.arrowX);
        var yLength = parseInt(y) - parseInt(this.arrowY);
        if (this.calcAngle(xLength,yLength) < 0.1 || this.calcAngle(xLength,yLength) > 2.2 ) {
            var width = parseInt(this.currentGlabWidth - (x - this.arrowX));
            this.setGlabWidth(width);
            this.moveX(parseInt(this.currentGlabX - (this.arrowX - x)));
            var newX = parseInt(this.currentGlabX - (this.arrowX - x)) + 15;
            var off = parseInt(newX) +15
            this.left.move(newX,off);
        }
    }

    /**
     * moves the right arrow of the current selected glab and resize the glab
     * @param {*} x
     * @param {*} y
     */
    moveRight(x,y) {
        if (this.calcAngle(x - this.arrowX,y-this.arrowY) < 0.1 || this.calcAngle(x-this.arrowX,y-this.arrowY)> 2.2) {
            var width = parseInt(this.currentGlabWidth-this.arrowX + x)
            this.setGlabWidth(width);
            x = parseInt(this.currentGlabX) + parseInt(this.glab.attr("width")) - 10
            var off = parseInt(x) -15
            this.right.move(x,off);
        }
    }

    /**
     * move the current selected glab
     * @param {*} dx how much to move horizontally
     * @param {*} dy how much to move vertically , will adapt to the corresponding staff
     */
    moveGlab(dx,dy) {
        var cadran = Math.PI/8
        var angle = this.calcAngle(dx,dy);
        //on travaille avec les multiples de pi/8 pour placer dans le cercle trigonométrique afin de déterminer la direction
        if (angle < 3*cadran || (angle > 5*cadran && angle < 11*cadran) || angle > 13*cadran) {
           this.moveX(this.x + parseInt(dx));
        }

        if ((angle > cadran && angle < 7*cadran) || (angle > 9*cadran && angle < 15*cadran)) {
            this.changeStaff(dy)
        }

        //pour adapter les flèches de selection
        this.stopSelection();
        this.selectCurrent();
    }

    /**
     * returns the nearest staff to y position
     * @param {*} y
     */
    getStaffAt(y) {
        for (var i = 0 ; i < this.staffs.length ; i++) {
            var staff = this.staffs[i]
            if (y < staff.getBottom() && y > staff.getTop()) {
                return i;
            }
        }
        return null;
    }

    /**
     * saves the values of the current selected glab
     * @param {*} x
     * @param {*} y
     */
    saveCurrentState(x,y) {
        this.arrowX = x;
        this.arrowY = y;
        this.currentGlabX = this.glab.attr("x");
        this.currentGlabWidth = this.glab.attr("width");
        return true;
    }

    /**
     * moves the current selected glab to the corresponding staff
     * @param {*} dy
     */
    changeStaff(dy) {
        var y = this.y + parseInt(dy)
        var currentStaff = this.getStaffAt(this.glab.attr("y"));
        var lim = null;
        var staff = this.staffs[currentStaff]
        if (dy < 0) {
            lim = (staff.getTop() + this.staffs[currentStaff -1].getBottom())/2;
            if (y < lim) {
                this.moveY(this.staffs[currentStaff - 1].getTop() + 10);
            }
        }
        else {
            lim = (staff.getBottom() + this.staffs[currentStaff +1].getTop())/2
            if (y > lim) {
                this.moveY(this.staffs[currentStaff + 1].getTop() + 10);
            }
        }
    }

}
