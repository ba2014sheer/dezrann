export class MeasureMusicalMap {
    constructor () {
        this._barPositions = [];
        this._ratio = 1;
        this._timeByMeasure = 4
        this._divByTime = 4
    }
    setBarsPositions (positions) {
        // position must be ordered
        this._barPositions = positions;
    }
    setRatio (r) {
        // 0 < r <= 1
        this._ratio = r;
    }
    toMusicalOffset (x) {
        let paperx = Math.round((x/this._ratio)*1000)/1000;
        let i = 0
        let start, end
        let bplen = this._barPositions.length
        if (paperx > this._barPositions[bplen-1]) { // x after last bar
            return -1
        }
        if (paperx < this._barPositions[0]) { // x before first bar
            return -1
        }
        if (paperx == this._barPositions[0]) { // x is on the first bar
            return 0.0
        }
        if (paperx == this._barPositions[bplen-1]) { // x is on the last bar
            return bplen*this._timeByMeasure
        }
        while ((paperx > this._barPositions[i]) && (i < bplen)) {
            i++
        }
        if (i >= bplen) { // unkown exception
            return -1
        }
        start = this._barPositions[i-1]
        end = this._barPositions[i]
        let exactOffset = (i-1+(paperx-start)/(end-start))*this._timeByMeasure;
        return Math.round(exactOffset*this._divByTime)/this._divByTime;

    }
    toMusicalDuration (startx, length) {
        let start = this.toMusicalOffset(startx)
        if (start == -1) {
            return -1;
        }
        let end = this.toMusicalOffset(startx+length)
        if (end == -1) {
            return -1;
        }
        return end - start
    }
    toGraphicalX (offset) {
        let lastOffset = (this._barPositions.length-1)*this._timeByMeasure
        if (offset > lastOffset) {
            return -1;
        } else if (offset == lastOffset) {
            return this._barPositions[this._barPositions.length-1]
        }
        let measureOffset = offset/this._timeByMeasure
        let i = Math.trunc(measureOffset)
        let portion = measureOffset - i
        let startx = this._barPositions[i]
        let endx = this._barPositions[i+1]
        return (startx + (endx-startx)*portion)*this._ratio;
    }
    toGraphicalLength (start, duration) {
        let leftEdge = this.toGraphicalX(start);
        if (leftEdge == -1) {
            return -1;
        }
        let rightEdge = this.toGraphicalX(start+duration);
        if (rightEdge == -1) {
            return -1;
        }
        return rightEdge - leftEdge;
    }
}

export class MusicalMap {
    constructor () {
        this._positions = [];
        this._ratio = 1;
        this._timeByMeasure = 4
        this._divByTime = 4
    }
    setPositions (positions) {
        this._positions = positions;
    }
    setRatio (r) {
        this._ratio = r;
    }
    _findDicho(element, first, last, from, to) {
        if (element <= this._positions[first][from]) {
            return this._positions[first][to]
        }
        if (element >= this._positions[last][from]) {
            return this._positions[last][to]
        }
        let index = Math.round((last-first)/2) + first
        let current = this._positions[index]
        let previous = this._positions[index-1]
        if (previous[from] <= element && element <= current[from]) {
            let middle = (previous[from] + current[from])/2
            if (element <= middle) {
                return previous[to]
            } else {
                return current[to]
            }
        } else if (element > current[from]) {
            return this._findDicho(element, index, last, from, to)
        } else if (element < previous[from]) {
            return this._findDicho(element, first, index, from, to)
        }
    }
    toMusicalOnset (x) {
        let paperx = Math.round((x/this._ratio)*1000)/1000;
        return this._findDicho(
            paperx,
            0,
            this._positions.length-1,
            "x",
            "onset"
        )
    }
    toMusicalDuration (startx, length) {
        return this.toMusicalOnset(startx+length) - this.toMusicalOnset(startx)
    }
    toGraphicalX(onset) {
        let x = this._findDicho(
            onset,
            0,
            this._positions.length-1,
            "onset",
            "x"
        )
        return Math.round(x*this._ratio);
    }
    toGraphicalLength (start, duration) {
        let begin, end;
        begin = this.toGraphicalX(start);
        end = this.toGraphicalX(start+duration);
        return end - begin;
    }

}
