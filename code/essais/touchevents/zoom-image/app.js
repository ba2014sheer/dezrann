var ratio; 

var setUpListeners = function() {
    var frame = document.getElementById("frame");
    frame.addEventListener("dragstart",transparentFrame);
    frame.addEventListener("dragend",fixFrame);

    frame.addEventListener("touchmove",fixFrameTouch);
    //frame.addEventListener("touchstart",triggerDrag);

    var zoom = document.getElementById("zoom");
    zoom.addEventListener("scroll",preventScrolling);

    ratio = document.getElementById("detail").clientWidth/window.innerWidth;
    
    frame.style.width = (1/ratio) * 100 + "%";
}



window.addEventListener("load",setUpListeners);

var transparentFrame = function(event) {
    event.srcElement.style.opacity = "0%";
}

var fixFrameTouch = function(event) {
    //only element changing from desktop web app 
    var x = event.touches[0].pageX;

    var zoom = document.getElementById("zoom");

    //so the frame doesn't go outside the screen 
    if (x < 0) {
        x = 0; 
    }

    if (x + ((1/ratio) * window.innerWidth) > window.innerWidth) {
        x = window.innerWidth - ((1/ratio) * window.innerWidth); 
    }

    this.style.left = x + "px";
    zoom.scrollLeft = x * ratio;
}

var fixFrame = function(event) {
    var x = event.pageX;
    var zoom = document.getElementById("zoom");

    //so the frame doesn't go outside the screen 
    if (x < 0) {
        x = 0; 
    }

    if (x + (0.15 * window.innerWidth) > window.innerWidth) {
        x = window.innerWidth - (0.15 * window.innerWidth); 
    }

    this.style.left = x + "px";
    zoom.scrollLeft = x * ratio;
}

var preventScrolling = function(event) {
   var val = event.srcElement.scrollLeft;
   document.getElementById("frame").style.left = (val/ratio) + "px";
}