var timer; 

var setUpListeners = function() {
    var button = document.getElementById("clickme");
    button.addEventListener("touchstart",touchManager);
    button.addEventListener("touchend",cancelTouch);

    //To see if touchstart is also considered as click 
    button.addEventListener("click",clickEvent);  
}

window.addEventListener("load",setUpListeners);

var clickEvent = function(event) {
    var p = document.createTextNode("J'ai cliqué sur le bouton \n");
    var br = document.createElement("br");
    document.getElementsByClassName("historic")[0].appendChild(p);
    document.getElementsByClassName("historic")[0].appendChild(br);
}

var longclick = function() {
    var p = document.createTextNode("J'ai cliqué LONGTEMPS sur le bouton \n");
    var br = document.createElement("br");
    document.getElementsByClassName("historic")[0].appendChild(p);
    document.getElementsByClassName("historic")[0].appendChild(br);
    return true; 
}

var touchManager = function(event) {
    window.setTimeout(longclick,1000);
}

var cancelTouch = function(event) {
    window.clearTimeout();
}