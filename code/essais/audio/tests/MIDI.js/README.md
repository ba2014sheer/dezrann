# MIDI.js - tests

Voici un projet permettant de tester la ressource MIDI.js.

Le document `keyboard.html` est un clavier de piano répondant au clique de la
souris.

Le document `midiplayer.html` est un player de fichier midi.

## Exécution

Pour télécharger les ressources nécessaires :

```shell
$ npm install
$ bower install
```

Pour exécuter le serveur http (port 3000) :

```shell
$ node_modules/.bin/gulpfile
```

Pour accéder au clavier : `http://localhost:3000/keyboard.html`

Pour accéder au player : `http://localhost:3000/midiplayer.html`

## Player midi

Le but du player midi est de réaliser un système répondant à plusieurs
critères :

1. Lire un fichier midi composé de plusieurs canaux joués simultanément
2. Mettre en pause et reprendre la lecture
3. Rendre un ou plusieurs canaux muet pendant la lecture
4. Accélérer le tempo

### Lecture d'un fichier midi

La lecture d'un fichier midi n'est pas compliqué. La première étape est de
charger les instruments souhaités. Ensuite il faut charger un fichier midi
grâce à la méthode `MIDI.loadFile()`. Losque le fichier est chargé il faut
attribuer à chaque canal de lecture un instrument. La lecture peut enfin
commencer en appellant la méthode `MIDI.start()`.

### Pause et reprise de lecture

Pour mettre en pause ou reprendre la lecture d'un fichier il faut appeller les
méthode `MIDI.pause()` et `MIDI.resume()`.

### Lecture de canal indépendant

Premier problème avec ce critère. Il semblerai que lors de l'appel de
`MIDI.start()` ou de `MIDI.resume()` MIDI.js ordonnance la lecture de la
totalité des notes du fichier (ou d'un certain nombre). Ce fonctionnement
implique qu'il n'est pas possible de "dès-ordonnancer" les notes d'un canal.
Pour le rendre muet, nous devons donc :

1. mettre en pause la lecture
2. rendre muet le canal
3. reprendre la lecture

Évidement ce comportement est audible et peu confortable pour l'utilisateur. Il
est possible d'observer ce comportement avec la lecture de "The Entertainer"
qui est composé de deux cannaux.

### Changer le tempo

Second problème. Comme évoqué précédement, les notes sont ordonnancée à la
lecture ce qui implique que le tempo doit être défini avant la lecture. Il
n'est donc pas possible de changer le tempo au cours de la lecture.

## Ressources utilisées

- Bower (Gestion des libraries côté client)
- NPM (Gestion des utilitaires de dévelopemment)
- Gulp (Gestion du transpiler)
- Babel, preset-2015, preset-2017 (Transpiler javascript)