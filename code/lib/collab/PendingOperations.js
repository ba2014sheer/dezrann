class PendingOperations {

  constructor () {
    this._operations = []
  }

  delete (id) {
    this._operations = this._operations.filter(o => o.op.id != id)
  }

  add (operation, serverVersion, applied = false, accepted = true) {
    this._operations.push({
      version : serverVersion,
      op : operation,
      applied : applied,
      accepted : accepted
    })
    this._operations.sort((o1, o2) => o1.version - o2.version)
  }

  addLocal (operation, version) {
    this.add(operation, version, true, false)
  }

  accept (id, version) {
    this._operations = this._operations.map(o => {
      if (o.op.id == id) {
        o.version = version
        o.accepted = true
      }
      return o
    }).sort((o1, o2) => o1.version - o2.version)
  }

  get allAccepted () {
    return this._operations.filter(o => !o.accepted).length == 0
  }

  get _accepted () {
    return this._operations.filter(o => o.accepted)
  }

  get oldestVersion () {
    return this._operations[0].version
  }

  /**
   * Remove operations from the a given version to the first version break
   * Returns these operations
   * ex :
   * this._operation = [(o1, v6), (o2, v7), (o3, v8), (o12, v7), ...]
   * => this._operations = [(o12, v7), ...]
   * Return [(o1, v6), (o2, v7), (o3, v8)]
   * @return {array} removed operations
   */
  purgeFrom (version) {
    let operations = this._accepted
    if (operations.length == 0) return []
    if (operations[0].version != version) return []
    let i = 0
    let v = operations[i].version
    while (operations[i] != undefined) {
      v = operations[i].version
      i++
    }
    let ret = operations.filter(o => o.version <= v)
    ret.forEach(o => this.delete(o.op.id))
    return ret
  }

  get length () {
    return this._operations.length
  }

  purge () {
    this._operations = []
  }

}

module.exports = PendingOperations;
