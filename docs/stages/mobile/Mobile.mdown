# Annoter les partitions musicales du bout des doigts

L'équipe Algomus développe une application web pour lire et annoter
des partitions musicales. Cette application utilise
le framework Polymer qui implémente la technologie des *WEB COMPONENTS*
(futur standard du W3C). Nous réalisons un ensemble de balises
HTML paramétrables qui s'intègrent aisément dans une page web à la manière des
balises HTML5 vidéo ou audio.

L'objectif du projet est de rendre
cet outil utilisable aussi bien sur tablette tactile ou smartphone que sur
ordinateur.
Le projet concernera aussi bien la gestion de l'affichage (taille de l'écran, composants limités ou adaptés)
que la gestion du tactile (flux d'événements sur plateforme mobile, réflexion et implémentation de la gestuelle).
Concrètement, le projet livrera une documentation technique et des nouveaux composants.

Mots clés: partitions musicales, IHM, Material design, Polymer, Web components,
javascript, snap.svg.

Liens
http://polymer-project.org