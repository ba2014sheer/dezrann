# ScoreCloud - Lexique

## les entités

* partition
  * milieu
  * naviguer
  * vue synthétique
  * fenêtre courante
  * drag and drop
  * mode edition
* schema ou vue synthétique
  * visible en entier
  * milieu
  * fenêtre courante
  * naviguer dans la partition
* analyse
  * vue synthétique
* pièce
* vue réduite
  * partition
* vue partition
* utilisateur
* lien vues schéma/partition
  * vue schéma
  * vue partition
  * vue analyse
* zone rectangulaire
* fenêtre courante de la partition
* vue entière réduite de la partition
* vue schema
* navigation
* mode édition
* drag an drop de la partition
* bouton de basculement de mode
* touche ctrl
* 2 doigts
* appui long
* label
* quadrillage invisible de la partition
* lignes
* portée
* colonnes
* positions des notes
* positions des barres de mesure
* zones où l'on ne peut pas dessiner
* inter-portée
* voix
* métrique principale
* métrique
* clef
* armure
* contrainte de dessin particulière
* label de portée
* couleur de label
* zone de portée
* point de départ
* point d'arrivée
* nom d'un label
* catégorie d'un label
* type de label
* Sujet
* Contre-sujet
* CS2
* CS3
* Thème principal
* Catalogue de catégories
* fichier truth
* custom type
* label global
* cadence
* curseur
* bouton de création de cadence
* mode création de cadence
* ligne verticale
* haut de partition
* bas de partition
* abscisse voulue
* Ajustement automatique (calage)
* Rectangle
* pédale
* coin du rectangle
* annotation génerale
* degré
* état de la navigation
* données de partition graphique
* partition image
* partition vexflow
* offset
* fichier image
* fichier de repérage
* données brute
* fichier lilypond
* reformatage du fichier lilypond
* fichier de macro lilypond
* autres formatages
* génération de l'image
* génération du repérage
* fichier krn
* fichier midi
* fichier musicXML
* fichier MEI
* image repérée
* génération du ly
* partition vexflow
* note
* audio
* zone principale
* zone secondaire
* disposition
* zone d'édition

